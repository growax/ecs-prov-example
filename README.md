# ecs-prov-example

Run this with assumption you have ansible and terraform installed on your machine.

Manual for creating EC2 instance and then install ElasticSearch on it.

1. Create and AWS User in a group with AmazonEC2FullAccess rights, then generate the credentials.

2. Store the AwS Credentials in the variables.tf file in terraform directory.

3. Create a Terraform plan main.tf to generate an AWS instance with Ubuntu 18.04.

4. Check Terraform plan works by running terraform plan command.

5. Create the infrastructure by running terraform apply.

6. Copy the public IP address from the Terraform output and put it on the Ansible hosts file in Ansible directory.

7. Run the Ansible script, ansible-playbook -i hosts install_elasticsearch.yml

8. curl to http://<aws_public_ip>:9200/ to test out elasticsearch.

The test should looks like this :

{
  "name" : "XXXXX",
  "cluster_name" : "<cluster_name>",
  "cluster_uuid" : "<cluster_uuid>",
  "version" : {
    "number" : "<version_number>",
    "build_flavor" : "default",
    "build_type" : "deb",
    "build_hash" : "XXXXX",
    "build_date" : "<build_date>",
    "build_snapshot" : false,
    "lucene_version" : "X.X.X",
    "minimum_wire_compatibility_version" : "X.X.X",
    "minimum_index_compatibility_version" : "X.X.X"
  },
  "tagline" : "You Know, for Search"
}

